import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { IconLookup, IconDefinition, findIconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MantineProvider, createTheme } from '@mantine/core';
import '@mantine/core/styles.css';
import '@mantine/dates/styles.css';
import '@mantine/tiptap/styles.css';
import Editor from '@/components/Editor';
import axios from 'axios';
import { useEffect } from 'react';

const theme = createTheme({
  /** Put your mantine theme override here */
});

library.add(fas);
const coffeeLookup: IconLookup = { prefix: 'fas', iconName: 'coffee' };
const coffeeIconDefinition: IconDefinition = findIconDefinition(coffeeLookup);

function App() {
  useEffect(() => {
    async function fetchData() {
      const foo = await axios.get('/resource');
      console.log(foo.data);
      const bar = await axios.get('/api/cars/3');
      console.log(bar.data);
    }
    fetchData();
  }, []);

  return (
    <MantineProvider theme={theme}>
      <div className="App">
        <div className="flex min-h-screen">
          <div className="m-auto grid gap-3">
            <div className="text-3xl font-bold">
              Hello react-template! <FontAwesomeIcon icon={coffeeIconDefinition} />
            </div>
            <Editor />
          </div>
        </div>
      </div>
    </MantineProvider>
  );
}

export default App;
