// 1. Import the "HttpResponse" class from the library.
import { http, HttpResponse } from 'msw';

export const handlers = [
  http.get('/resource', () => {
    // 2. Return a mocked "Response" instance from the handler.
    return HttpResponse.text('Hello world!');
  }),
  http.get('/apples', () => {
    return new HttpResponse(null, {
      status: 404,
      statusText: 'Out Of Apples',
    });
  }),
  http.post('/auth', () => {
    return new HttpResponse(null, {
      headers: {
        'Set-Cookie': 'mySecret=abc-123',
        'X-Custom-Header': 'yes',
      },
    });
  }),
  http.get('/name', () => {
    return new HttpResponse('John');
  }),
  http.post('/auth2', () => {
    // Note that you DON'T have to stringify the JSON!
    return HttpResponse.json({
      user: {
        id: 'abc-123',
        name: 'John Maverick',
      },
    });
  }),
  http.get('/user', () => {
    // Respond with "401 Unauthorized" to "GET /user" requests.
    return new HttpResponse(null, { status: 401 });
  }),
  http.post('/checkout/cart', () => {
    return HttpResponse.error();
  }),
];
