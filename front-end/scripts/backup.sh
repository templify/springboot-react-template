#!/bin/bash

echo "Bash script"

today=$(date +"%Y-%m-%d %H%M")
projectRootPath=$(pwd | sed 's/\/scripts$//')

# Ensure the backup directory exists
mkdir -p "$projectRootPath/backup"
mkdir -p "$projectRootPath/backup/$today"

rsync -a --exclude=node_modules --exclude=build --exclude=backup --exclude=storybook-static --exclude=dist "$projectRootPath" "$projectRootPath/backup/$today/template"
echo "complete rsync"

cd "$projectRootPath/backup/$today" || exit

tar -czf template.tar template
echo "complete tar"

rm -rf template
echo "complete rm"

cd "$projectRootPath/scripts" || exit