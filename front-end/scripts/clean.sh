#!/bin/bash

echo "Bash script"

projectRootPath=$(pwd | sed 's/\/scripts$//')

cd "$projectRootPath" || exit

rm -rf node_modules npm_packages yarn.lock storybook-static dist

yarn cache clean

cd "$projectRootPath/scripts" || exit