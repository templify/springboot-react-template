echo "powershell script";

$today = Get-Date -Format "yyyy-MM-dd HHmm";
$projectRootPath = (Get-Location).path.replace('\scripts', '')

robocopy $projectRootPath $projectRootPath/backup/$today/template /E /TEE /XD "node_modules" "npm_packages" "build" "backup" "storybook-static" "dist";
echo "complete robocopy";

Set-Location $projectRootPath/backup/$today

tar -cvzf template.tar template;
echo "complete tar";
 
Remove-Item -Recurse -Force -Path template;
echo "complete Remove-Item";

Set-Location $projectRootPath/scripts