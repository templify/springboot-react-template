package com.car.database;

import com.car.database.domain.Car;
import com.car.database.domain.CarRepository;
import com.car.database.domain.Owner;
import com.car.database.domain.OwnerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class CarDatabaseApplication implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(CarDatabaseApplication.class);

    @Autowired
    private CarRepository repository;

    @Autowired
    private OwnerRepository orepository;

    public static void main(String[] args) {
        SpringApplication.run(CarDatabaseApplication.class, args);
        logger.info("Application started successfully.");
    }

    @Override
    public void run(String... args) throws Exception {
        Owner owner1 = new Owner("John", "Johnson");
        Owner owner2 = new Owner("Mary", "Robinson");
        orepository.saveAll(Arrays.asList(owner1, owner2));

        repository.save(new Car("Ford", "Mustang", "Red", "ADF-1121", 2017, 59000, owner1));
        repository.save(new Car("Nissan", "Leaf", "White", "SSJ-3002", 2014, 29000, owner1));
        repository.save(new Car("Toyota", "Prius", "Silver", "KKO-0212", 2018, 39000, owner1));
        repository.save(new Car("Honda", "Accord", "Black", "KKO-0212", 2018, 39000, owner1));
        repository.save(new Car("audi", "a4", "Blue", "KKO-0212", 2018, 39000, owner2));

        for (Car car : repository.findAll()) {
            logger.info(car.getBrand() + " " + car.getModel());
        }

        logger.error("this is an error message");
        for (Car car : repository.findByBrandEndsWith("a")) {
            logger.error(car.getBrand() + " " + car.getModel());
        }
    }
}
