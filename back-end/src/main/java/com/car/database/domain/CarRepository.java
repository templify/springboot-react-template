package com.car.database.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface CarRepository extends CrudRepository<Car, Long> {

    List<Car> findByBrandOrderByYearAsc(String brand);

    List<Car> findByBrandOrColor(String brand, String color);

    List<Car> findByBrandAndModel(String brand, String model);

    @Query("select c from Car c where c.brand = ?1")
    List<Car> findByBrand(String brand);

    //    List<Car> findByColor(String color);
    List<Car> findByColor(@Param("color") String color);

    List<Car> findByYear(int year);

    @Query("select c from Car c where c.brand like %?1")
    List<Car> findByBrandEndsWith(String brand);


}